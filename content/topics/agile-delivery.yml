---
  title: What is Agile delivery?
  description: Learn about the benefits of Agile Delivery, and how this iterative approach to software delivery is used to deliver better quality code, faster!
  topics_breadcrumb: true
  topics_header:
    data:
      title: What is Agile delivery?
      updated_date:
      block:
        - text: |
            Just as Agile project management brings an iterative approach to project management, Agile delivery is an iterative approach to software delivery in which teams build software incrementally at the beginning of a project rather than shipping it all at once upon completion.
          link_text: "Learn how to accelerate delivery"
          link_href: https://learn.gitlab.com/agile-topic/201906-whitepaper-re#page=1
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Agile delivery
  side_menu:
    anchors:
      text: "On this page"
      data:
      - text: What are Agile proccesses?
        href: "#what-are-agile-processes"
        data_ga_name: what are agile processes
        data_ga_location: side-navigation
      - text: The key principles of Agile software delivery
        href: "#the-key-principles-of-agile-software-delivery"
        data_ga_name: the key principles of agile software delivery
        data_ga_location: side-navigation
      - text: Agile delivery basics and benefits
        href: "#agile-delivery-basics-and-benefits"
        data_ga_name: agile delivery basics and benefits
        data_ga_location: side-navigation
      - text: Agile delivery frameworks
        href: "#agile-delivery-frameworks"
        data_ga_name: agile-delivery-frameworks
        data_ga_location: side-navigation
      - text: What makes Agile software delivery so effective?
        href: "#what-makes-agile-software-delivery-so-effective"
        data_ga_name: what makes agile software delivery so effective
        data_ga_location: side-navigation
      - text: What are common challenges with Agile delivery
        href: "#what-are-common-challenges-with-Agile-delivery"
        data_ga_name: what are common challenges with agile delivery
        data_ga_location: side-navigation
      - text: Why embrace Agile processes?
        href: "#why-embrace-agile-processes"
        data_ga_name: why embrace agile processes
        data_ga_location: side-navigation
      - text: How can I get started with Agile software development?
        href: "#how-can-i-get-started-with-agile-software-development"
        data_ga_name: the dev ops lifecycle and how dev ops works
        data_ga_location: side-navigation
    hyperlinks:
      text: "More on this topic"
      data:
        - text: GitLab's ultimate guide to Agile project & portfolio management (PPM)
          href: https://about.gitlab.com/topics/agile-delivery/agile-ppm/
          data_ga_location: side-navigation
          data_ga_name: GitLab's ultimate guide to Agile project & portfolio management (PPM)
          variant: tertiary
          icon: true
    content:
      - name: topics-copy-block
        data:
          header: What are Agile processes?
          column_size: 10
          blocks:
            - text: | 
                Agile is one of the most important and transformative methodologies introduced to the software engineering discipline in recent decades, helping software teams efficiently create customer-centric products.
            - text: | 
                Agile development means taking iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects. It is based on the Agile Manifesto, which values individuals and interactions, working software, customer collaboration, and responding to change.
            - text: 
                Agile project management processes are an alternative to traditional project management approaches such as the Waterfall or Spiral methodologies, which assume a linear path from one stage to another. Agile brings this to the next level by empowering teams to not only accept but embrace a non-linear workflow that changes and evolves during the development process. Agile processes emphasize building working products quickly and iteratively, and focus on empowering cross-functional teams rather than establishing top-down hierarchies.
      - name: topics-copy-block
        data:
          header: The key principles of Agile software delivery
          column_size: 10
          blocks:
              - text: |
                  By rapidly responding to changes and adapting their plans accordingly, Agile delivery teams are able to deliver high-quality products and services quickly and efficiently. These benefits are achieved through the application of the four key principles of Agile delivery:

                  1. **Collaboration**: Agile methodologies value people and human interactions over processes and tools. Agile approaches help teams keep the focus on team members by allowing communication to happen in a fluid, natural way as the need arises. And when team members can communicate freely and naturally, they can collaborate more effectively.
                  2. **Customer focus**: The highest priority of Agile teams, as outlined in the Agile Manifesto, is “to satisfy the customer through early and continuous delivery of valuable software.” In other words, it all comes down to delivering better products to customers more quickly.
                  3. **Value-based prioritization**: The process of prioritization — determining what should be done now and what can be done later — is a core principle of the Scrum methodology, a popular agile framework. Prioritization allows teams to deliver the most possible value to customers in the shortest amount of time.
                  4. **Iterative development**: In Agile delivery, tasks are broken down into smaller deliverables that can be repeated and refined throughout the software development cycle. This allows teams to constantly review their progress and identify opportunities for improvement.

      - name: topics-copy-block
        data:
          header: Agile delivery basics and benefits
          column_size: 10
          blocks:
              - text: |
                  The basics of Agile delivery include having an Agile mindset and creating an Agile enviorment, aftwards you're team will be able to immediately reap the benefits which include but are not limited to a faster speed to market, higher quality code and transparency.

                    ### Agile mindset

                  An Agile mindset means viewing setbacks as learning opportunities, embracing iteration, collaboration, and change, and focusing on delivering value. With an Agile mindset, teams can adjust to changing market needs, respond to customer feedback, and deliver business value. Adopting a new perspective can positively change a team’s culture, since the shift permits innovation without fear, collaboration with ease, and delivery without roadblocks.

                    ### Agile environment

                  An Agile environment is a workplace that is designed to support Agile processes. Agile environments favor individuals and interactions over processes and tools, working software over comprehensive documentation, customer collaboration over contract negotiation, and responding to change over following a plan. An Agile environment encourages team members to work collaboratively and promotes constant innovation and process improvement.

                    ### Speed to market

                  Faster time to market enables quicker customer feedback and higher customer satisfaction.

                    ### Higher quality

                  Since testing is integrated throughout the lifecycle, teams have early sight into quality issues.

                    ### Transparency

                  Teams are involved throughout a project — from planning and prioritizing to building and deploying.
      - name: topics-copy-block
        data:
          header: Agile delivery frameworks?
          column_size: 10
          blocks:
            - text: |
                  There are many different agile delivery frameworks, but some of the most common are Scrum, Kanban, and Lean. Each of these frameworks has its own unique set of values, principles, and practices that help guide organizations in their transition to an agile way of working.

                    ### Scrum


                    Scrum, often synonymous with Agile, is an approach that emphasizes continuous improvement, self organization, and experience-based learning. By utilizing user stories, tasks, backlogs, and extensions, teams have a structured model to carry them across a software development lifecycle. Teams that use a Scrum approach to development are likely to be committed, respectful, and focused.


                    ### Kanban


                    Teams that use a Kanban framework favor transparency and communication. Tasks are organized using Kanban cards on a board to enable end-to-end visibility throughout production. Three practices guide Kanban: visualize work, limit work in progress, and manage flow. Teams that use a Kanban framework are collaborative, transparent, balanced, and customer focused.


                    ### Lean


                    Lean software development comes from lean manufacturing principles and practices and focuses on eliminating waste, amplifying learning, deciding as late as possible, delivering as fast as possible, empowering the team, building integrity in, and optimizing the whole.
      - name: topics-copy-block
        data:
          header: What makes Agile software delivery so effective?
          column_size: 10
          blocks:
            - text: |
                There are a number of reasons why Agile software delivery methods are becoming increasingly popular. First, they allow for much more flexibility and responsiveness to change than traditional waterfall methods. Organizations that are successful with Agile software delivery methods have clear business priorities and engage users and feedback in active delivery refinements.


                  Core Agile methodology elements within the software delivery process help make it a success.


                  * Teams are kept small and iterations short

                  * Feedback from customers is fast

                  * Business priorities are value-based

                  * Users are engaged in the refining of end-product requirements


                Agile methods tend to focus on delivering value to the end user, rather than simply meeting internal deadlines or milestones. Value-based business priorities and engaging users in refining requirements are key to making agile software delivery methods work effectively.
      - name: topics-copy-block
        data:
          header: What are common challenges with Agile delivery?
          column_size: 10
          blocks:
            - text: |
               Agile delivery can be a great way to improve your software development process, but it can also present some challenges. For example, you may need to change the way you communicate with stakeholders, or you may need to adjust your project management approach. You may also find that you need to invest in new tools and training for your team.

                Common challenges with Agile approaches include:


                * Constant feedback and collaboration between the customer and the development team in order to deliver a more flexible and responsive product

                * New tools to manage and the need to integrate the Agile structure and methodology across teams and stakeholders

                * Team members need to be trained in and aware of Agile concepts in order to improve performance and streamline processes


                If you’re willing to overcome these challenges, you’ll likely find that Agile delivery can help you speed up your development process and improve your software quality.
      - name: topics-copy-block
        data:
          header: Why embrace Agile processes?
          column_size: 10
          blocks:
            - text: |
                The demand for faster software development is universal, and [Agile delivery meets both customer and business needs](/solutions/agile-delivery/).

                Organizations that adopt Agile practices can gain a competitive edge in a fast-changing market. Businesses that empower teams to use Agile processes satisfy discerning customers and adapt to new technologies, helping them to develop the products that set the standard for industries.

                It’s not just businesses that benefit from Agile delivery. Customers have more substantive experiences with organizations when their needs are met and their feedback makes a difference in product development. Customers appreciate when their input and expectations help shape an organization’s releases.
      - name: topics-copy-block
        data:
          header: How can I get started with Agile software development?
          column_size: 10
          blocks:
            - text: |
                If you’re looking to [get started with Agile software development](/blog/2018/03/05/gitlab-for-agile-software-development/), there are a few things you can do. First, you’ll need to identify what your Agile software development and delivery process will look like. You’ll also need to consider what your team’s goals are, what your customers’ needs are, and what your delivery timeline looks like.
            - text: | 
                Once you have a good understanding of all of these factors, you can begin to put together your Agile development and delivery process. Once structured and implemented, you will then need to monitor your development process to ensure it is working as required, and if not, optimize as necessary.
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: webcast-alt
              variant: marketing
              alt: Webcast Icon
            event_type: "Webcast"
            header: Getting started with GitLab issues
            link_text: "Watch now"
            image: "/nuxt-images/topics/devops/how-devops-leads-transformation.jpeg"
            href: https://learn.gitlab.com/agile-topic/watch-40
            data_ga_name: Getting started with GitLab issues
            data_ga_location: resource cards
          - icon:
              name: webcast-alt
              variant: marketing
              alt: Webcast Icon
            event_type: "Webcast"
            header: Getting started with GitLab epics
            link_text: "Watch now"
            image: "/nuxt-images/topics/devops/cloud-native-devops.jpeg"
            href: https://learn.gitlab.com/agile-topic/watch-41
            data_ga_name: Getting started with GitLab epics
            data_ga_location: resource cards
          - icon:
              name: webcast-alt
              variant: marketing
              alt: Webcast Icon
            event_type: "Webcast"
            header: Scaled Agile Framework (SAFe) with GitLab
            link_text: "Watch now"
            image: "/nuxt-images/topics/devops/devops-tips-and-tricks.jpeg"
            href: https://www.youtube.com/watch?v=PmFFlTH2DQk
            data_ga_name: Scaled Agile Framework (SAFe) with GitLab
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: Axway was able to achieve hourly deployments with GitLab CI/CD
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_3.jpg"
            href: https://about.gitlab.com/customers/axway-devops/
            data_ga_name: Axway was able to achieve hourly deployments with GitLab CI/CD
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: How GitLab CI supported Ticketmaster's ramp up to weekly mobile releases
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_1.jpeg"
            href: https://about.gitlab.com/blog/2017/06/07/continuous-integration-ticketmaster/
            data_ga_name: How GitLab CI supported Ticketmaster's ramp up to weekly mobile releases
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "Agile planning"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_10.jpeg"
            href: /solutions/agile-delivery/
            data_ga_name: "Agile planning"
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "Scaled Agile and GitLab"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_11.jpeg"
            href: https://about.gitlab.com/solutions/agile-delivery/scaled-agile/
            data_ga_name: "Scaled Agile and GitLab"
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "Accelerating software delivery"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_14.jpeg"
            href: https://about.gitlab.com/solutions/agile-delivery/scaled-agile/
            data_ga_name: "Accelerating software delivery"
            data_ga_location: resource cards

    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to use GitLab for Agile software development"
            text: |
                  How Agile artifacts map to GitLab features and how an Agile iteration looks in GitLab.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "How to use GitLab for Agile software development"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "What is an Agile mindset?"
            text: |
                  Learn how embracing change can help you speed up software delivery.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/06/13/agile-mindset/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "What is an Agile mindset?"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "4 ways to use GitLab Issue Boards"
            text: |
                  By leveraging the power of labels, GitLab Issue Boards can be easily customized to support any workflow. Here are four examples.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
            image: /nuxt-images/blogimages/scm-ci-cr.png
            data_ga_name: "4 ways to use GitLab Issue Boards"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Issue labels can now be scoped!"
            text: |
                  A small change with a huge impact: Scoped Labels can help teams customize their workflow and speed up delivery.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/06/20/issue-labels-can-now-be-scoped/
            image: /nuxt-images/blogimages/markus-spiske-MkwAXj8LV8c-unsplash.png
            data_ga_name: "Issue labels can now be scoped!"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "5 Ways to stay agile in a growing organization"
            text: |
                  Some of the GitLab Manage team have a conversation about staying agile as a company grows.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/06/10/manage-conversation-staying-agile/
            image: /nuxt-images/blogimages/future-of-software-future-proof-your-career.png
            data_ga_name: "5 Ways to stay agile in a growing organization"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Improving pair programming with pairing sessions"
            text: |
                  Pairing with a teammate can increase delivery. Here we look at what pairing sessions are, what they involve and what they're good for.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/08/20/agile-pairing-sessions/
            image: /nuxt-images/blogimages/whatisgitlabflow.jpg
            data_ga_name: "Improving pair programming with pairing sessions"
            data_ga_location: resource cards
  schema_faq:
    - question: What is Agile delivery?
      answer: |
        Agile delivery is an iterative approach to software delivery in which
        teams build software  incrementally at the beginning of a project rather
        than ship it at once upon completion.


        [Learn more about Agile Delivery](https://about.gitlab.com/topics/agile-delivery/#what-is-agile-delivery:~:text=What%20is%20Agile%20delivery%3F)
    - question: What is a DevOps platform?
      answer: |
        Businesses that empower teams to use Agile development practices satisfy
        discerning customers and adapt to new technologies, helping them to
        develop the products that set the standard for industries.


        [Learn more about embracing Agile delivery](https://about.gitlab.com/topics/agile-delivery/#why-embrace-agile-delivery:~:text=Why%20embrace%20Agile%20delivery)
