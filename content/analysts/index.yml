---
  title: Industry Analyst Research
  description: What top industry analysts are saying about GitLab
  components:
    - name: 'analysts-hero'
      data:
        title: What top industry analysts are saying about GitLab
        text: Discover what third-party industry analysts are saying about GitLab so that through their in-depth research on how GitLab solves customer challenges, you can learn how The DevSecOps Platform sets you up for success in the evolving technology landscape.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          url: /nuxt-images/solutions/infinity-icon-cropped.svg
          alt: "Image: gitlab partners"
    - name: 'featured-reports'
      data:
        title: Featured reports
        metadata:
          id_tag: featured-reports
        reports:
          - name: 2022 Gartner® Magic Quadrant™ for Application Security Testing
            description: This Magic Quadrant evaluates 14 application security testing (AST) vendors.
            logo: /nuxt-images/logos/gartner-logo.svg
            img_url: /nuxt-images/analysts/ast-mq-22.png
            img_alt: gartner quadrant
            logo_alt: gartner logo
            cta_url: /analysts/gartner-ast22/
          - name: 2021 Gartner® Market Guide for Value Stream Delivery Platforms
            description: GitLab Cited as Representative Vendor in 2021 Market Guide
            logo: /nuxt-images/logos/gartner-logo.svg
            img_url: /nuxt-images/analysts/gartner-report.png
            img_alt: gartner report
            logo_alt: gartner logo
            cta_url: /analysts/gartner-vsdp21/
            cta_data_ga_name: gartner market guide
            cta_data_ga_location: reports
          - name: 'The 2019 Forrester Wave™: Cloud-Native Continuous Integration Tools'
            description: Forrester names GitLab a leader for Continuous Integration in their 2019 evaluation
            logo: /nuxt-images/logos/forrester-logo.svg
            img_url: /nuxt-images/analysts/forrester-report.png
            img_alt: forrester report
            logo_alt: forrester logo
            cta_url: /analysts/forrester-cloudci19/
            cta_data_ga_name: forrester magic report ci tools
            cta_data_ga_location: reports
    # - name: 'review'
    #   data:
    #     title: Share your voice
    #     text: Your expert insights help others make the right technology and business decisions. 
    #     link_text: Write a review
    #     link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
    #     image: /nuxt-images/analysts/team-collaboration.png
    #     alt:  Team meeting 
    - name: 'market-research'
      data:
        title: Market research of interest
        metadata:
          id_tag: market-research
        reports:
          - name: 2021 Gartner® Magic Quadrant for Enterprise Agile Planning Tools
            logo: /nuxt-images/logos/gartner-logo.svg
            logo_alt: gartner logo
            img_url: /nuxt-images/analysts/gartner-quadrant.png
            img_alt: gartner quadrant
            cta_url: /analysts/gartner-eapt21/
            cta_data_ga_name: gartner magic quadrant agile planning
            cta_data_ga_location: reports
          - name: 2021 Gartner® Magic Quadrant for Application Security Testing
            logo: /nuxt-images/logos/gartner-logo.svg
            logo_alt: gartner logo
            img_url: /nuxt-images/analysts/gartner-security-quadrant.png
            img_alt: gartner security quadrant
            cta_url: /analysts/gartner-ast21/
            cta_data_ga_name: gartner magic quadrant application security
            cta_data_ga_location: reports
    - name: "accordion-feature-list"
      data:
        title: "Categories of analysts reports relevant to GitLab"
        groups:
        - group_name: "Report categories relevant to GitLab"
          items:
            list1:
            - title: API Security Testing
            - title: Application Performance Monitoring (APM)
            - title: Application Security Testing (AST) / Static Application Security Testing
            - title: Browser-based IDEs
            - title: Container Management / Public Cloud Container Services
            - title: Design Management
            list2:
            - title: DesignOps
            - title: DevOps
            - title: DevSecOps
            - title: Enterprise Agile Planning Tools (EAPT)
            - title: GitOps / Infrastructure as Code (IAC)
            - title: Hypothesis-Driven Development
            - title: Incident Response / Incident Managment
            list3:
            - title: Integrated Software Development Platforms
            - title: IT Infrastructure Monitoring
            - title: MLOps / ModelOps
            - title: Observability
            - title: Performance Engineering
            - title: Progressive Delivery
            - title: Software Composition Analysis (SCA)
            list4:
            - title: Software Supply Chain Security
            - title: Value Stream Delivery Platforms (VSDP)
            - title: Value Stream Management (VSM)
            - title: Value Stream Management Platforms (VSMP)
            - title: Zero Trust Security
        - group_name: "Additional categories of interest"
          items:
            list1:
              - title: AIOps
              - title: Application Portfolio Management
              - title: Continuous Autonomous Validation and Verification   
              - title: Digital Experience Management (DEM)
            list2:  
              - title: Innersourcing
              - title: Low-Code Application Platforms / Low-code Development Platforms / Low-Code Development Technologies
              - title: IT Service Management (ITSM) 
            list3:
              - title: Multiexperience Development Platforms (MXDP)
              - title: Project & Portfolio Management (PPM) / Strategic Portfolio Management
            list4:
              - title: Service Orchestration and Automation Platforms (SOAP)
              - title: Security Orchestration, Automation and Response (SOAR) 
    - name: 'analysts-contacts'
      data:
        title: Analyst relations contacts
        cards:
          - name: Gaby Berkman
            job: Analyst Relations Manager
            email: gberkman@gitlab.com
            phone: 1-802-318-5682
            avatar: /nuxt-images/icons/avatar_purple.png
          - name: Ryan Ragozzine
            job: Analyst Relations Manager
            email: rragozzine@gitlab.com
            phone: 1-619-871-7497
            avatar: /nuxt-images/icons/avatar_purple.png
          - name: Natasha Woods
            job: Director, Corporate Communications
            email: nwoods@gitlab.com
            phone: 1-415-312-5289
            avatar: /nuxt-images/icons/avatar_purple.png
        
    # - name: 'collapsible-feature-list'
    #   data:
    #     title: Reports mentioning GitLab
    #     groups:
    #     - logo: /nuxt-images/logos/forrester-logo.svg
    #       alt: Forrester
    #       items: 
    #           - title: "Streamline Process Management With Lean And Agile Thinking, February 8, 2022"
    #           - title: "Charter A Corporate Cloud Platform Team, January 20, 2022"
    #           - title: "Build Better Teams By Playing Video Games At Work, December 30, 2021"
    #           - title: "How To Make Hybrid Work, November 15, 2021"
    #           - title: "The Forrester Guide To Hybrid Cloud, September 16, 2021"
    #           - title: "The Forrester Wave™: Software Composition Analysis, Q3 2021, August 18, 2021"
    #           - title: "Modern Development Metrics That Really Matter, August 2, 2021"
    #           - title: "Now Tech: Software Composition Analysis, Q2 2021, April 7, 2021"
    #           - title: "Seize The Anywhere-Work Opportunity By Taking Calculated Risks, March 29, 2021"
    #           - title: "China Tech Market Outlook, 2021 To 2022, January 14, 2021"
    #           - title: "The Forrester Wave™: Static Application Security Testing, Q1 2021, January 11, 2021"
    #           - title: "Harness ChatOps To Empower Remote Collaboration, December 21, 2020"
    #           - title: "COVID Drives M&A Activity In DevOps And IT Management, December 4, 2020"
    #           - title: " Use The Lessons Of 2020 To Create Your Anywhere-Work Strategy, November 7, 2020"
    #           - title: "Europe’s Digital Renaissance Starts Now, November 11, 2020"
    #           - title: "API Insecurity: The Lurking Threat In Your Software, October 22, 2020"
    #           - title: "The Forrester Tech Tide™: Application Security, Q4 2020, October 8, 2020"
    #           - title: "The Business Of Belonging, September 16, 2020"
    #           - title: "The Forrester Guide To Configuration Management, September 11, 2020"
    #           - title: "Rethink Your CMDB, September 11, 2020"
    #           - title: "Build Your Operations Organization With Product Team Principles, September 9, 2020"
    #           - title: "Now Tech: Static Application Security Testing, Q3 2020, August 6, 2020"
    #           - title: "The Forrester Wave™: Value Stream Management Solutions, Q3 2020, July 15, 2020 "
    #           - title: "The State Of Remote Work, 2020, July 6, 2020"
    #           - title: "Now Tech: Zero Trust Solution Providers, Q2 2020, Updated May 27, 2020"
    #           - title: "Now Tech: Value Stream Management Tools, Q2 2020, April 29, 2020"
    #           - title: "Now Tech: Continuous Delivery And Release Automation, Q2 2020, April 9, 2020"
    #           - title: "It's Go Time For Application And Infrastructure Dependency Mapping (AIDM), February 7, 2020"
    #     - logo: /nuxt-images/logos/gartner-logo.svg
    #       alt: Gartner
    #       items: 
    #           - title: "Managing Machine Identities, Secrets, Keys and Certificates, March 16, 2022"
    #           - title: "Technology Insight for Digital Product Design Platforms, March 7, 2022"
    #           - title: "Assessing Red Hat OpenShift Container Platform for Cloud-Native Application Delivery on Kubernetes, February 28, 2022"
    #           - title: "Quick Answer: How Should We Manage Our Atlassian Contract?, February 23, 2022"
    #           - title: "Quick Answer: How to Create a Frictionless Onboarding Experience for Software Engineers, February 11, 2022"
    #           - title: "Innovation Insight for Continuous Compliance Automation, February 11, 2022"
    #           - title: "Gartner Peer Connect Perspectives: What Are Product Owners’ Roles and Responsibilities and Whom Should They Report To?, January 19, 2022"
    #           - title: "How to Deploy and Perform Application Security Testing, December 21, 2021"
    #           - title: "Product Owner Essentials, December 14, 2021"
    #           - title: "Predicts 2022: Modernizing Software Development is Key to Digital Transformation, December 3, 2021"
    #           - title: "Emerging Technology Horizon for Enterprise Software, 2021, November 30, 2021"
    #           - title: "Drive Innovation by Enabling Innersource, November 29, 2021"
    #           - title: "Market Guide for Value Stream Management Platforms, November 10, 2021"
    #           - title: "Solution Comparison for Low-Code Application Platforms, October 18, 2021"
    #           - title: "Magic Quadrant for Industrial IoT Platforms, October 18, 2021"
    #           - title: "2022 Planning Guide for IT Operations and Cloud Management, October 11, 2021"
    #           - title: "Emerging Technologies and Trends Impact Radar: Enterprise Software, October 4, 2021"
    #           - title: "Quick Answer: How Should We Test Enterprise SaaS Applications?, September 29, 2021"
    #           - title: "Magic Quadrant for Full Life Cycle API Management, September 28, 2021"
    #           - title: "Using Emerging Service Connectivity Technology to Optimize Microservice Application Networking, September 21, 2021"
    #           - title: "Critical Capabilities for Enterprise Low-Code Application Platforms, September 21, 2021"
    #           - title: "Market Guide for Software Composition Analysis, September 14, 2021"
    #           - title: "Gartner Peer Insights 'Voice of the Customer': Enterprise Agile Planning Tools, September 9, 2021"
    #           - title: "Innovation Insight for Cloud-Native Application Protection Platforms, August 25, 2021"
    #           - title: "The Innovation Leader's Guide to Navigating the Cloud-Native Container Ecosystem, August 25, 2021"
    #           - title: "Hype Cycle for Enterprise Architecture, 2021, August 6, 2021"
    #           - title: "Hype Cycle for Software Engineering, 2021, July 27, 2021"
    #           - title: "Hype Cycle for Open-Source Software, 2021, July 26, 2021"
    #           - title: "Hype Cycle for ITSM, 2021, July 21, 2021"
    #           - title: "Hype Cycle for I&O Automation, 2021, July 16, 2021"
    #           - title: "How Software Engineering Leaders Can Mitigate Software Supply Chain Security Risks, July 15, 2021"
    #           - title: "Hype Cycle for Communications Service Provider Operations, 2021, July 14, 2021"
    #           - title: "Hype Cycle for ICT in China, 2021, July 13, 2021"
    #           - title: "Hype Cycle for Agile and DevOps, 2021, July 12, 2021"
    #           - title: "Hype Cycle for Application Security, 2021, July 12, 2021"
    #           - title: "Market Share Analysis: Application Development Software, Worldwide, 2020, July 7, 2021"
    #           - title: "Solution Path for Continuous Delivery With DevOps, June 16, 2021"
    #           - title: "2 Important Steps to Drive Action From Your Procurement Policy, June 4, 2021"
    #           - title: "The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams, June 4, 2021"
    #           - title: "Magic Quadrant for Application Security Testing, May 27, 2021"
    #           - title: "Solution Criteria for AIOps Platforms, May 27, 2021"
    #           - title: "Critical Capabilities for Application Security Testing, May 26, 2021"
    #           - title: "Vendor Rating: Amazon, May 17, 2021"
    #           - title: "Cool Vendors in Cloud Computing, May 13, 2021"
    #           - title: "How Software Engineering Leaders Can Use Value Stream Metrics to Improve Agile Effectiveness, May 4, 2021"
    #           - title: "Ignition Guide to Updating Your Procurement Policy, May 3, 2021"
    #           - title: "Cool Vendors for Software Engineering Technologies, May 3, 2021"
    #           - title: "Innovation Insight for Continuous Infrastructure Automation, April 27, 2021"
    #           - title: "Critical Capabilities for Enterprise Agile Planning Tools, April 21, 2021"
    #           - title: "Magic Quadrant for Enterprise Agile Planning Tools, April 20, 2021"
    #           - title: "12 Things to Get Right for Successful DevSecOps, April 9, 2021"
    #           - title: "Tool: Open-Source Software Governance Policy Template, March 16, 2021"
    #           - title: "Assessing Site Reliability Engineering (SRE) Principles for Building a Reliability-Focused Culture, January 28, 2021"
    #           - title: "Solution Criteria for Hyperconverged Infrastructure Software, January 27, 2021"
    #           - title: "To Automate Your Automation, Apply Agile and DevOps Practices to Infrastructure and Operations, January 6, 2021"
    #           - title: "How to Automate Server Provisioning and Configuration Management, December 23, 2020"
    #           - title: "Emerging Technology Horizon for Enterprise Software, December 17, 2020"
    #           - title: "Four Steps to Adopt Open-Source Software as Part of Your Test Automation Stack, December 16, 2020"
    #           - title: "Magic Quadrant for Enterprise Architecture Tools, December 14, 2020"
    #           - title: "Implementing an Enterprise Open-Source Machine Learning Stack, December 3, 2020"
    #           - title: "How to Choose Your Best-Fit Vendor for Test Management, December 2, 2020"
    #           - title: "Ensure Safe and Successful Usage of Open-Source Software With a Comprehensive Governance Policy, November 20, 2020"
    #           - title: "Tech CEOs Can Expand the Container-Based Infrastructure Market by Supporting Edge Computing and Machine Learning, November 19, 2020"
    #           - title: "Market Trends: The Rise of Cloud-Native Technology Ecosystems (Container Perspective), November 2, 2020"
    #           - title: "Emerging Technologies and Trends Impact Radar: Enterprise Software, October 16, 2020"
    #           - title: "Gartner Peer Insights ‘Voice of the Customer’: Enterprise Agile Planning Tools, October 13, 2020"
    #           - title: "Gartner Peer Insights ‘Voice of the Customer’: Application Security Testing, October 9, 2020"
    #           - title: "2021 Planning Guide for IT Operations and Cloud Management, October 9, 2020"
    #           - title: "Market Guide for DevOps Value Stream Delivery Platforms, 28 September, 2020"
    #           - title: "Four Steps to Adopt Open-Source Software as Part of the DevOps Toolchain, updated 19 August, 2020"
    #           - title: "Competitive Landscape: Custom Software Development Services, 19 August, 2020"
    #           - title: "Market Guide for Software Composition Analysis, 18 August, 2020"
    #           - title: "Analyze Value Stream Metrics to Optimize DevOps Delivery, 7 August, 2020 "
    #           - title: "Hype Cycle for I&O Automation, 2020, 4 August, 2020"
    #           - title: "Hype Cycle for Application Architecture and Development, 2020, 31 July, 2020"
    #           - title: "Emerging Technologies: Functionality Spectrum for Cloud Workload Protection Platforms, 2020, 30 July, 2020"
    #           - title: "CTO’s Guide to Containers and Kubernetes — Answering the Top 10 FAQs, 27 July, 2020"
    #           - title: "Hype Cycle for ITSM, 2020, 27 July, 2020"
    #           - title: "Hype Cycle for Application Security, 2020, 27 July, 2020"
    #           - title: "Hype Cycle for Agile and DevOps, 2020, 15 July, 2020"
    #           - title: "Hype Cycle for Open-Source Software, 2020, 10 July, 2020"
    #           - title: "Vendor Rating: Amazon, 7 July, 2020"
    #           - title: "How to Build an Effective Remote Testing Competency, 30 June, 2020"
    #           - title: "Structuring Application Security Tools and Practices for DevOps and DevSecOps, 18 June, 2020"
    #           - title: "7 Tips to Set Up an Application Security Program Without Breaking the Bank, 11 June, 2020"
    #           - title: "Guidance Framework for Choosing What to Automate to Increase Application Delivery Agility, 4 June, 2020"
    #           - title: "Guidance Framework for Deploying Centralized Log Monitoring, 26 May, 2020"
    #           - title: "Solution Path for Evolving to Next-Generation Enterprise Networks, 21 May, 2020"
    #           - title: "Cool Vendors in Agile and DevOps, 20 May, 2020"
    #           - title: "3 Steps to Sustain Productivity and Collaboration in Remote Agile and DevOps Teams, 6 May, 2020"
    #           - title: "Magic Quadrant for Application Security Testing, 29 April, 2020"
    #           - title: "Critical Capabilities for Enterprise Agile Planning Tools, 29 April, 2020"
    #           - title: "Critical Capabilities for Application Security Testing, 27 April, 2020"
    #           - title: "Magic Quadrant for Enterprise Agile Planning Tools, 21 April, 2020"
    #           - title: "Best Practices to Enable Continuous Delivery With Containers and DevOps, 16 April, 2020"
    #           - title: "Setting Up Remote Agile in a Hurry?, 2 April, 2020"
    #           - title: "How Application Leaders Can Address the Unprecedented Organizational and Cultural Impacts of COVID-19, 31 March, 2020"
    #           - title: "How to Deploy and Perform Application Security Testing, 20 March, 2020"
    #           - title: "Gartner Peer Insights ‘Voice of the Customer’: Application Release Orchestration, 12 March, 2020"
    #           - title: "Assessing HashiCorp Terraform for Provisioning Cloud Infrastructure, 6 March, 2020"
    #           - title: "Essential Skills for Automation Engineers, 26 February, 2020"
    #           - title: "How to Automate Your Network Using DevOps Practices and Infrastructure as Code, 15 January, 2020"
    #           - title: "The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams, 14 January, 2020"
    #     - logo: /nuxt-images/logos/idc-logo.svg
    #       alt: Idc
    #       items: 
    #           - title: "The Impact of the Russia-Ukraine War on the Global ICT Market Landscape — IDC's First Take, March 4, 2022"
    #           - title: "Worldwide Development Languages, Environments, and Tools Market Shares, 2020: Growth Increases as Enterprises Accelerate Digital Innovation Due to COVID-19, December, 2021"
    #           - title: "IDC's Worldwide Enterprise Infrastructure Workloads Taxonomy, 2H21, November, 2021"
    #           - title: "IDC FutureScape: Worldwide Developer and DevOps 2022 Predictions, October, 2021"
    #           - title: "IDC Market Glance: DevOps, 3Q21, August, 2021"
    #           - title: "Worldwide DevOps Software Tools Market Shares, 2020: Growth Fueled by Accelerated Digital Transformation, July, 2021"
    #           - title: "Worldwide DevSecOps Software Tools Market Shares, 2020: Strong Growth as DevOps Teams Prioritize Security, July, 2021"
    #           - title: "Worldwide Automated Software Quality Market Shares, 2020: Rapid Digitalization Impels Ongoing Quality Adoption, June, 2021"
    #           - title: "IDC TechBrief: GitOps, June, 2021"
    #           - title: "IDC's Worldwide Software Taxonomy, 2021, April, 2021"
    #           - title: "IDC's Worldwide Intelligent CloudOps Software Taxonomy, 2021, February, 2021"
    #           - title: "IBM Partners with GitLab on DevOps Automation Platform, January, 2021"
    #           - title: "Leveraging DevOps Delivery for Rapid Responsiveness and Digital Innovation Moving into the 'New Normal', December, 2020"
    #           - title: "Worldwide Software Change, Configuration, and Process Management Forecast Update, 2020–2024: Adaptive Software Demand Drives SCCPM Forecast Growth, December, 2020"
    #           - title: "Understanding the Role and Impact of Open Core Open Source Software Development, December, 2020"
    #           - title: "IDC Market Glance: Application Development, 4Q20, December, 2020"
    #           - title: "IDC FutureScape: Worldwide Developer and DevOps 2021 Predictions, October, 2020"
    #           - title: "IDC FutureScape: Worldwide Future of Digital Innovation 2021 Predictions, October, 2020"
    #           - title: "GitLab — The Incremental Building of a DevOps Platform, August, 2020"
    #           - title: "Worldwide DevOps Software Tools Market Shares, 2019: Leaders Drive Double-Digit Growth, July, 2020"
    #           - title: "IDC Market Glance: Collaborative Innovation, 2Q20, June, 2020"
    #           - title: "IDC Market Glance: Service Mesh Ecosystem, 1H20, June, 2020"
    #           - title: "IBM Think 2020 — Pervasive AI Accelerates Business Transformation, June, 2020"
    #           - title: "IDC Market Glance: DevOps Application Life-Cycle Management, 1Q20, March, 2020"
    #           - title: "JFrog Announces a New Unified DevOps Platform, February, 2020"
    #           - title: "IDC MarketScape: Worldwide Agile Project and Portfolio Management 2020 Vendor Assessment — Enabling Business Velocity for Digital Innovation, January 2020"
    #           - title: "IDC Market Glance Update: Future of Work, 1Q20, January, 2020"
  
